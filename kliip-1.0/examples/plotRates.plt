#!/bin/gnuplot
set term post enhanced eps "Times-Roman, 14"
set output "rates.eps"
set yrange [9E-10:]
set xrange [0:250]
set logscale y
set format y '%.0e'
set multiplot layout 2,1
plot "fort.11" u 1:3 w l title "Q_{rad}",\
     "" u 1:4 w l title "Q_{cond}",\
     "" u 1:5 w l title "Q_{sub}"
plot "fort.11" u 1:2 w l title "Q_{abs}",\
     "" u 1:6 w l title "Q_{ann}",\
     "" u 1:7 w l title "Q_{ox}"
unset multiplot

! open rates.eps

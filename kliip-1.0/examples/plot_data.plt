#!/bin/gnuplot
####!/usr/bin/gnuplot
set term post enhanced eps "Times-Roman" 14 fontscale 1.0
set output "LaserProfile_Temperature.eps"
set ylabel "Temperature [K]"
set xlabel "Time [ns]"
set y2tics nomirror
set xrange [0:60]
#set xrange [0:60]
scale = 1000000000.

plot "output.dat" u ($1*scale):2 axes x1y1 w l lt 2 title "Particle Temperature",\
     "laser.dat" u 1:2 axes x1y2 w l lt 1 title "Laser Profile"

set output "LII_Signal.eps"

unset ytics
unset y2tics
set ylabel "LII Signal"
plot "output.dat" u ($1*scale):5 axes x1y1 w l lt 1 title "Signal",\
     "laser.dat" u 1:2 axes x1y2 w l lt 2 title "Laser Profile"

set output "rates.eps"

set multiplot layout 2,1
set ytics
set format y '%.1e'
set logscale y
set xrange [0:60]
set ylabel 'Energy loss rates [J/s]'

#plot "heatTransferRates.dat" u 1:3 w l lt 1 title "Qrad",\
#     "heatTransferRates.dat" u 1:4 w l lt 2 title "Qcond",\
#     "heatTransferRates.dat" u 1:5 w l lt 3 title "Qsub"
#set ylabel 'Energy production rates [J/s]'
#plot "heatTransferRates.dat" u 1:2 w l lt 1 title "Qabs"

unset multiplot
#! evince LII_Signal.eps LaserProfile_Temperature.eps
#! evince LaserProfile_Temperature.eps 
#! open LII_Signal.eps LaserProfile_Temperature.eps #rates.eps
! open LaserProfile_Temperature.eps

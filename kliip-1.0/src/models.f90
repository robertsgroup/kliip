module models
implicit none

contains

!----------------------------------------------------------!
!
! Subroutines and functions for Laser Absorption modeling
!
!----------------------------------------------------------!

  subroutine computeCabs(Cabs,phase)
    use data
    implicit none
    real(dp),intent(out)::Cabs
    integer,intent(in)::phase
    Cabs = ((pi**2)*((particle%D(k))**3)*E(lambda,phase))/lambda
  end subroutine computeCabs

  function q(time)
    use data, only : dp,q_exp,len_lsrprof,F,q_exp_int,tol,q_e
    use errorCheck
    implicit none
    integer :: i
    real(dp) :: q
    real(dp) :: time 
    real(dp) :: c1,c2,c3
    real(dp) :: q_tmp
    
    do i=1,(len_lsrprof-1) 
      if((time>=q_exp(i,1)) .and. (time<=(q_exp(i+1,1)+tol))) then
        c1 = q_exp(i+1,2)-q_exp(i,2) 
        c2 = time-q_exp(i,1)
        c3 = q_exp(i+1,1)-q_exp(i,1)
        q_tmp = q_exp(i,2)+c1*(c2/c3)
        q_e = q_tmp
        print *,'in here' 
        q = (F*q_tmp)/q_exp_int
        print *,q,abs(q)
        exit
      !elseif(abs(q_exp(i,1)-time) > tol) then
      !  c1 = q_exp(i+1,2)-q_exp(i,2) 
      !  c2 = time-q_exp(i,1)
      !  c3 = q_exp(i+1,1)-q_exp(i,1)
      !  q_tmp = q_exp(i,2)+c1*(c2/c3)
      !  q = (F*q_tmp)/q_exp_int 
      !  exit
      elseif(i==(len_lsrprof-1)) then
        error = 101
        call errorFunc
        !q_tmp = 0.0d0
        !q = (F*q_tmp)/q_exp_int 
      endif
    enddo
    if(ISNAN(q)) then
      error = 100 
      call errorFunc 
    endif
    if(q<0.0d0) then
      call errorFunc
    endif
  end function

  function E(lambda,phase)
    use data, only : dp
    implicit none
    real(dp),intent(in)::lambda
    integer,intent(in)::phase
    real(dp) :: E
    real(dp) :: c1,c2,c3

    E = 0.0d0
    c1 = 6.0d0*nm(lambda,phase)*km(lambda,phase)
    c2 = (((nm(lambda,phase))**2)-((km(lambda,phase))**2)+2)**2 
    c3 = 4.0d0*((nm(lambda,phase))**2)*((km(lambda,phase))**2) 
    E = c1/(c2+c3)
  end function 
   
  function nm(lambda,phase)
    use data, only : dp
    implicit none
    real(dp) :: lambda
    real(dp) :: nm
    integer :: phase
    if(phase == 0) then
      ! unannealed phase
      nm = 2.213d0 + 9.551d3*lambda
    else 
      ! annealed phase
      nm = 1.26d0
    endif
  end function

  function km(lambda,phase)
    use data, only : dp
    implicit none
    real(dp) :: lambda
    real(dp) :: km
    integer :: phase
    if(phase == 0) then 
      ! unannealed phase
      km = 0.7528d0 + 1.265d4*lambda
    else
      ! annealed phase
      km = 0.364d0+4.784d2*lambda+6.06d7*(lambda**2)
    endif
  end function

!----------------------------------------------------------!
!
! Subroutines and functions for Radiation modeling
!
!----------------------------------------------------------!
   
  function l(eta)
    use data, only : dp 
    implicit none 
    real(dp) :: eta
    real(dp) :: l
    !l = (4.0d0/3.0d0)*GAMMA(4.0d0+eta)*Z(40.d0+eta)
    l=(4.0d0/3.0d0)*EXP(1.862d0+1.99d0*(eta)+0.1909d0*(eta**2)&
        -0.02968d0*(eta**3))
  end function

!----------------------------------------------------------!
!
! Subroutines and functions for Conduction modeling
!
!----------------------------------------------------------!

  function Z_surf(T,P)
    use data, only : dp,kp,R_m,pi,W_a
    implicit none
    real(dp) :: T,P 
    real(dp) :: Z_surf
    real(dp) :: c1,c2
    c1 = P/(kp*T)
    c2 = sqrt((R_m*T)/((2.0d0)*pi*W_a))
    Z_surf = c1*c2
  end function
end module models

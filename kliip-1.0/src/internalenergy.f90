module internalEnergy
implicit none

contains
subroutine computeQbulk(layer)
    use data, only : dp,T,Qbulk,delta
    use thermo, only : kappas
    implicit none
    integer,intent(in) :: layer
    real(dp) :: c1,c2,c3
    real(dp) :: radius

    radius = delta*(real(layer)-1.0d0)
    c1 = kappas(T)*dT2dr2(layer)
    if(layer==1) then
      c2 = 2.0d0*kappas(T)*dT2dr2(layer)
    else
      c2 = (2.0d0*kappas(T)/(radius))&
           *dTdr(layer)
    endif
    c3 = dKappasdT(T)*(dTdr(layer)**2)
    Qbulk = c1 + c2 + c3
  end subroutine

  function dTdr(i)
    use data, only : dp,k_layer,delta,T_layer
    implicit none
    integer,intent(in) :: i
    real(dp) :: dTdr
    if(i==1) then
      dTdr = 0.0d0
    else
      dTdr = (T_layer(k_layer,i+1)-T_layer(k_layer,i-1))/(2.0d0*delta)
    endif
  end function

  function dT2dr2(i)
    use data, only : dp,k_layer,delta,T_layer
    implicit none
    integer,intent(in) :: i
    real(dp) :: dT2dr2
    real(dp) :: c1

    if(i==1) then
      c1 = 2.0d0*(T_layer(k_layer,i+1)-T_layer(k_layer,i))
      dT2dr2 = c1/(delta**2)
    else
      c1 = T_layer(k_layer,i+1)-(2.0d0*T_layer(k_layer,i))+T_layer(k_layer,i-1)
      dT2dr2 = c1/(delta**2)
    endif
   end function
  function dKappasdT(temp)
    use data, only : dp
    implicit none
    real(dp) :: temp
    real(dp) :: dKappasdT
    !integer :: layer
    dkappasdT = -1.0d0*(16.985d0/(temp**2))
  end function

end module

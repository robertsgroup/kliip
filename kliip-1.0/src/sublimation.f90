module sublimation
implicit none

contains
!----------------------------------------------------------!
!
! Subroutine for sublimation modeling
!
!----------------------------------------------------------!

  subroutine computeQsub
    use data, only: dp,T,Qsub,delH_lambda_a,delH_lambda_s
    implicit none
    real(dp) :: c1,c2,c3,c4,c5
    integer :: j
    Qsub = 0.0_dp
    do j = 1,10
      c1 = (1.0_dp/Wj(j))*dMdt(j)
      c2 = delHj(T,j)*(P_sat_Cj(T,j)-P_phot_Cj(T,j))
      c3 = delH_lambda_s(j)*P_lambda(T,j,1)
      c4 = delHj(T,2)*Pdiss(T,j)+delH_lambda_a(j)*P_lambda(T,j,2)
      c5 = c1*((c2+c3+c4)/P_sat_Cj(T,j))
      Qsub = Qsub + c5
    enddo
    Qsub = -1.0d0*Qsub
  end subroutine

  function dMdt(j)
    use data, only : dp,pi,R_p,T,D
    implicit none
    real(dp) :: dMdt
    real(dp) :: c1,c2,c3
    real(dp) :: temp
    integer :: j
    temp = T
    c1 = -pi*D*D*Wj(j)
    c2 = Uj(temp,j)*alphaj(j)*Bj(temp,j)
    c3 = R_p*temp
    dMdt = (c1*c2)/c3
  end function

  function Uj(temp,j)
    use data, only : dp,pi,R_m
    implicit none
    real(dp) :: Uj
    real(dp) :: temp
    integer :: j
    Uj = sqrt((R_m*temp)/(2.0_dp*pi*Wj(j)))
  end function

  function Wj(j)
    use data, only : dp,W_1
    implicit none
    real(dp) :: Wj
    integer :: j
    Wj = real(j)*W_1
  end function

 function alphaj(j)
    use data, only : dp
    implicit none
    real(dp) :: alphaj
    integer :: j
    if((j==1) .or. (j==2)) then
      alphaj = 0.5_dp
    elseif(j==3) then
      alphaj = 0.1_dp
    else
      alphaj = 1.0E-4_dp
    endif
  end function

  function Bj(temp,j)
    use data, only : dp,P_o,D
    implicit none
    real(dp) :: Bj
    real(dp) :: temp
    real(dp) :: c1,c2,c3
    integer :: j
    if((P_sat_Cj(temp,j)/P_o)<(3.0E-3_dp)) then
      c1 = 2.0_dp*Deff(j)*P_sat_Cj(temp,j)
      c2 = D*alphaj(j)*Uj(temp,j)
      c3 = 2.0_dp*Deff(j)
      Bj = c1/(c2+c3)
    else
      Bj = P_sat_Cj(temp,j)-P_surf_Cj(temp,j)
    endif
  end function

  function Deff(j)
    use data, only : dp,Xa
    implicit none
    real(dp) :: Deff
    integer :: j
    j = j
    Deff = Deff_C(3)*(1.0_dp-Xa)+Deff_C(2)*Xa
  end function

  function Deff_C(j)
    use data, only : dp,pi,R_m,P_o,tol,T,T_bath,sigmaj,A0,A1,kp
    implicit none
    integer :: j
    real(dp) :: Deff_C
    real(dp) :: temp
    real(dp) :: c1,c2,c3,c4,c5
    temp = T
    c1 = (A0(j)/A1(j))
    c2 = sqrt(R_m/(pi*Wj(j)))
    c3 = (3.0_dp*fcrt(j)*kp*temp)/(4.0_dp*sigmaj(j)*P_o)
    c4 = ((temp+c1)**2)-((T_bath+c1)**2)
    c5 = (sqrt(temp)*(temp+3.0_dp*c1))-(sqrt(T_bath)*(T_bath+3.0_dp*c1))
    if(c4<tol) then
      Deff_C = 0.0_dp
    else
      Deff_C = c3*c2*(c4/c5)
    endif
  end function

  function gammaj(j)
    use data, only : dp
    implicit none
    integer :: j
    real(dp) :: gammaj
    if(j==1) then
      gammaj = 1.4_dp
    elseif(j==2) then
      gammaj = 1.3_dp
    elseif(j==3) then
      gammaj = 1.2_dp
    elseif(j==4) then
      gammaj = 1.1_dp
    else
      gammaj = 1.0_dp
    endif
  end function

  function fcrt(j)
    use data, only : dp
    implicit none
    real(dp) :: fcrt
    integer :: j
    fcrt = (9.0_dp*gammaj(j)-5.0_dp)/4.0_dp
  end function

  function etafunc(temp,j)
    use data, only : dp,P_o
    implicit none
    real(dp) :: etafunc
    real(dp) :: temp
    real(dp) :: tmp
    integer :: j
    integer :: i
    tmp = 0.0d0
    do i = 1,10
      tmp = tmp + P_sat_Cj(temp,i)
    enddo
    etafunc = (P_o+tmp)/P_sat_Cj(temp,j)
  end function

  function zfunc(temp,j)
    use data, only : dp,D,P_o 
    implicit none
    real(dp) :: zfunc
    real(dp) :: temp
    real(dp) :: c1,c2
    integer :: j
    c1 = (D*alphaj(j)*Uj(temp,j))/(2.0_dp*Deff(j))
    c2 = 1.0_dp-((etafunc(temp,j)*P_sat_Cj(temp,j))/P_o)
    zfunc = c1*exp(c1*c2)
  end function

  function Jfunc(z)
    use data, only : dp
    implicit none
    real(dp) :: Jfunc
    real(dp) :: z
    if(z<=3.0d-3) then
      Jfunc = z
    elseif((3.0d-3<z) .and. (z<0.05d0)) then
      Jfunc = (0.92267d0)*z**(0.98535d0)
    elseif((0.05d0<=z) .and. (z<=300.0d0)) then
      Jfunc = (5.815d-1)+(3.496d-1)*log(z)+(6.516d-2)&
              *(log(z)**2)+(1.667d-3)*(log(z)**3)&
              +(6.031d-4)*(log(z)**4)-(4.161d-5)*(log(z)**5)
    else
      Jfunc = (-1.189d0)+(9.187d-1)*(log(z))
    endif
  end function

  function P_surf_Cj(temp,j)
    use data, only : dp,D,P_o
    implicit none
    real(dp) :: P_surf_Cj
    real(dp) :: temp
    real(dp) :: c1,c2
    integer :: j
    c1 = P_o/(etafunc(temp,j))
    c2 = (2.0_dp*Deff(j))/(D*alphaj(j)*Uj(temp,j))
    P_surf_Cj = c1*(1.0_dp-c2*Jfunc(zfunc(temp,j)))
  end function

  function P_sat_Cj(temp,j)
    use data, only : dp
    implicit none
    real(dp) :: P_sat_Cj
    real(dp) :: temp
    integer :: j
    if(P_equil_Cj(temp,j)>P_phot_Cj(temp,j)) then
      P_sat_Cj = P_equil_Cj(temp,j)
    else
      P_sat_Cj = P_phot_Cj(temp,j)
    endif
  end function

  function k_lambda(j,i)
    use data
    implicit none
    real(dp) :: k_lambda
    real(dp) :: c1,c2,c3
    integer :: j,i
    
    if(i==1) then
      c1 = sigmaj(j)*pi*(D**3)*Nss
      c2 = (1.0_dp-Xa-Xm)
      c3 = 1.0_dp-exp(-Blambda_s*(F**n)*(q_e**n))
      k_lambda = ((c1*c2)/6.0_dp)*c3
    else
      c1 = sigmaj(j)*pi*(D**3)*Nsa
      c2 = Xa
      c3 = 1.0_dp-exp(-Blambda_a*(F**n)*(q_e**n))
      k_lambda = ((c1*c2)/6.0_dp)*c3
    endif
  end function

  function P_equil_Cj(temp,j)
    use data, only : dp,R,Pref
    implicit none
    real(dp) :: P_equil_Cj
    real(dp) :: temp
    real(dp) :: c1,c2
    integer :: j
    c1 = delSj(temp,j)/R
    c2 = delHj(temp,j)/(R*temp)
    P_equil_Cj = Pref*exp(c1-c2)
  end function

 function P_phot_Cj(temp,j)
    use data, only : dp
    implicit none
    real(dp) :: P_phot_Cj
    real(dp) :: temp
    integer :: j
    P_phot_Cj = P_lambda(temp,j,1)+Pdiss(temp,j)+P_lambda(temp,j,2)
  end function

  function P_lambda(temp,j,i)
    use data, only : dp,kp,D,pi
    implicit none
    real(dp) :: P_lambda
    real(dp) :: temp
    real(dp) :: c1,c2
    integer :: i,j
    c1 = kp*temp*k_lambda(j,i)
    c2 = pi*(D**2)*alphaj(j)*Uj(temp,j)
    P_lambda = c1/c2
  end function

  function Pdiss(temp,j)
    use data, only : dp,pi,N_p,W_1,N_a,kp,Xa,M,D,Adiss,Ediss
    use thermo, only : k_rxn
    implicit none
    real(dp) :: Pdiss
    real(dp) :: temp
    real(dp) :: c1,c2
    integer :: j
    N_p = (M*N_a)/W_1
    c1 = kp*temp*k_rxn(Adiss,Ediss,temp)*Xa*N_p
    c2 = 2.0_dp*pi*(D**2)*alphaj(j)*Uj(temp,j)
    Pdiss = c1/c2
  end function

  function delHj(temp,j)
    use data, only : dp,b0H,b1H,b2H
    implicit none
    real(dp) :: delHj
    real(dp) :: temp
    integer :: j

    delHj = b0H(j)+(b1H(j)*temp)+(b2H(j)*(temp**2))

  end function

  function delSj(temp,j)
    use data, only : dp,b0S,b1S,b2S
    implicit none
    real(dp) :: delSj
    real(dp) :: temp
    integer :: j

    delSj = b0S(j)+(b1S(j)*temp)+(b2S(j)*(temp**2))

  end function
end module

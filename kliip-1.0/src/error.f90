module errorCheck
use io
implicit none
integer :: error
contains

  subroutine errorFunc
    implicit none
    character(len=100) :: error_out_file
    print *,
    print *,' ***WARNING*** ' 
    print *,
    print *,'Dumping output data to data.log'
    print *,
    error_out_file = 'data.log' 
    call writeData(error_out_file)
    if((error>199) .and. (error<300)) then
      print *,' Error in SOLVE module'
      print *
      if(error==200) then
         print *,' Fatal Error -> in updateT subroutine' 
         print *,' Computed particle temperature is NAN'
         print *
         stop
      endif
    elseif((error>0) .and. (error<200)) then
      print *,' Error in MODELS module'
      print *
      if((error>99) .and. (error<110)) then
        print *,' Error in Subroutines and functions for Laser Absorption modeling'
        print *
        if(error==100) then
          print *,' Fatal Error -> in q function'
          print *,' Computed q value is NAN'
          print *
          stop
        elseif(error==101) then
          print *,' Fatal Error -> in q function'
          print *,' Current time value outside laser profile data'
          print *
        endif
      endif
    elseif((error>299) .and. (error<500)) then
      print *,' Error in HEATTRANSFER module'
      print *
      if((error>299) .and. (error<325)) then
        print *,' Error in laser absorption heat transfer model'
        print *
        if(error==300) then
          print *,' Fatal Error -> Absorption Module'
          print *,' Error with -> computeQabs subroutine'
          print *,' Computed Qabs value is NAN'
          print *
          stop
        elseif(error==301) then
          print *,' Fatal Error -> computeQabs subroutine'
          print *,' Computed Qabs value is negative'
          print *
        endif
      endif
    elseif((error>499) .and. (error<1000)) then
      print *,' Error in THERMO module'
      print *
      if((error>499) .and. (error<505)) then
        print *,' -Error in RHOS function'
        if(error==500) then
          print *,'  Fatal Error -> rhos function'
          print *,'  Computed rhos value is negative'
          print *
          stop
        endif
      endif
      if((error>504) .and. (error<510)) then
        print *,' -Error in RHOL function'
        if(error==505) then
          print *,'  Fatal Error -> rhol function'
          print *,'  Computed rhol value is negative'
          print *
          stop
        endif
      endif
      if((error>509) .and. (error<515)) then
        print *,' -Error in RHOA function'
        if(error==510) then
          print *,'  Fatal Error -> rhoa function'
          print *,'  Computed rhoa value is negative'
          print *
          stop
        endif
      endif
    endif    
  end subroutine

end module

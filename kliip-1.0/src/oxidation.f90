module oxidation
implicit none

contains
  subroutine computeQox
    use data, only: dp,Qox,N_a,alpha_T,delH_ox,pi, &
                     D,T,neg
    use thermo, only: C_pco
    implicit none
    real(dp) :: c1,c2
    c1 = neg*delH_ox - 2.0_dp*alpha_T*C_pco(T)*T
    c2 = (pi*(D**2)*k_ox(T))/N_a 
    Qox = c1*c2
  end subroutine
  function k_ox(temp)
    use data, only: dp,P_O2,Aa,Ea,Az,Ez,Ab,Eb 
    use thermo, only: k_rxn
    implicit none
    real(dp) :: temp
    real(dp) :: k_ox
    real(dp) :: c1,c2,c3,c4
    real(dp) :: T
    T = temp 
    c1 = 12.0_dp*P_O2
    c2 = k_rxn(Aa,Ea,T)*chi_A(T)
    c3 = 1.0_dp+k_rxn(Az,Ez,T)*P_O2
    c4 = k_rxn(Ab,Eb,T)*(1.0_dp-chi_A(T))
    k_ox = c1*((c2/c3)+c4)
  end function k_ox
  function chi_A(T)
    use data, only: dp,At,Et,Ab,Eb,P_O2
    use thermo, only: k_rxn
    implicit none
    real(dp) :: chi_A
    real(dp),intent(in) :: T
    chi_A = 1.0_dp/(1.0_dp + (k_rxn(At,Et,T)/(k_rxn(Ab,Eb,T)*P_O2))) 
  end function
  function dMdt_ox(D,T)
    use data, only: dp,W_1,N_a,Pi,neg 
    implicit none
    real(dp),intent(in) :: D
    real(dp),intent(in) :: T
    real(dp) :: dMdt_ox
    dMdt_ox = neg*(2.0_dp*pi*(D**2)*W_1*k_ox(T))/N_a 
    !phrint *,D,W_1,k_ox(T),T,dMdt_ox
    !stop
  end function
end module oxidation

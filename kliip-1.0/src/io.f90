module io
implicit none

contains
  subroutine readinputfile()
    use data
    implicit none
    integer :: status
    integer :: maxlines = 10000
    integer :: i,j
    character(len=50) :: tmp
    character(len=50),dimension(10) :: readline
     
    print *,
    print *,' Reading input file ', input_file

    open(unit=1,file=trim(adjustl(input_file)),status='old',action='read',iostat=status)
    rewind(1)
    do i = 1, maxlines
      if(status/=0) exit
      read(1,'(A)',iostat=status) tmp
      if(status/=0) exit
      backspace 1
      read(1,*) (readline(j), j=1,ntokens(trim(tmp)))
      !if(status/=0) exit
      !read(1,*,iostat=status) tmp1, tmp2, tmp3
      !print *,i,trim(tmp1),' ',trim(tmp2),' ',trim(tmp3)
      if(trim(readline(1))=='absorption') then
        read(1,*) tmp
        Qabs_on=(tmp=='on').or.(tmp=='On').or.(tmp=='ON').or.(tmp =='oN')
      elseif(trim(readline(1))=='radiation') then
        read(1,*) tmp
        Qrad_on=(tmp=='on').or.(tmp=='On').or.(tmp=='ON').or.(tmp =='oN')
      elseif(trim(readline(1))=='conduction') then
        read(1,*) tmp
        Qcond_on=(tmp=='on').or.(tmp=='On').or.(tmp=='ON').or.(tmp =='oN')
      elseif(trim(readline(1))=='sublimation') then
        read(1,*) tmp
        Qsub_on=(tmp=='on').or.(tmp=='On').or.(tmp=='ON').or.(tmp =='oN')
      elseif(trim(readline(1))=='annealing') then
        read(1,*) tmp
        Qann_on=(tmp=='on').or.(tmp=='On').or.(tmp=='ON').or.(tmp =='oN')
      elseif(trim(readline(1))=='oxidation') then
        read(1,*) tmp
        Qox_on=(tmp=='on').or.(tmp=='On').or.(tmp=='ON').or.(tmp =='oN')
      elseif((trim(readline(1))=='primary') .and. (trim(readline(2))=='particle') .and. &
            (trim(readline(3))=='diameter')) then
        read(1,*) D
        D = D*(1.0d-7)
      elseif((trim(readline(1))=='excitation') .and. (trim(readline(2))=='wavelength')) then
        read(1,*) lambda
        lambda = lambda*(1.0d-7)
      elseif((trim(readline(1))=='simulation') .and. (trim(readline(2))=='end') .and. & 
              (trim(readline(3))=='time')) then
        read(1,*) time_final
        time_final = time_final*(1.0d-9)
      elseif(trim(readline(1))=='fluence') then
        read(1,*) F
      elseif((trim(readline(1))=='initial') .and. (trim(readline(2))=='particle') .and. &
              (trim(readline(3))=='temperature')) then
        read(1,*) T
      elseif((trim(readline(1))=='bath') .and. (trim(readline(2))=='gas') .and. &
              (trim(readline(3))=='temperature')) then
        read(1,*) T_bath 
      elseif((trim(readline(1))=='global') .and. (trim(readline(2))=='time') .and. &
               (trim(readline(3))=='step')) then
        read(1,*) dt
        dt = dt*(1.0d-12)
      elseif((trim(readline(1))=='internal') .and. (trim(readline(2))=='time') .and. &
               (trim(readline(3))=='step')) then
        read(1,*) dt_layer
        dt_layer = dt_layer*(1.0d-12)

      elseif((trim(readline(1))=='internal') .and. (trim(readline(2))=='layer') .and. &
              (trim(readline(3))=='thickness')) then
        read(1,*) delta
        delta = delta*(1.0d-7)
      elseif((trim(readline(1))=='output') .and. (trim(readline(2))=='file')) then
        read(1,*) out_file
      elseif((trim(readline(1))=='ambient') .and. (trim(readline(2))=='pressure')) then
        read(1,*) P_o
      elseif((trim(readline(1))=='laser') .and. (trim(readline(2))=='profile')) then
        read(1,*) LsrProf
      elseif((trim(readline(1))=='output') .and. (trim(readline(2))=='file')) then
        read(1,*) out_file
      elseif((trim(readline(1))=='collection') .and. (trim(readline(2))=='wavelength') .and. &
               (trim(readline(3))=='range')) then
        read(1,*) colLambdai, colLambdaj
        colLambdai = colLambdai*1.0d-7
        colLambdaj = colLambdaj*1.0d-7
      endif
    enddo
    close(1)
  end subroutine readinputfile
  integer function ntokens(line)
  character,intent(in):: line*(*)
  integer i, n, toks

  i = 1;
  n = len_trim(line)
  toks = 0
  ntokens = 0
  do while(i <= n)
     do while(line(i:i) == ' ') 
       i = i + 1
        if (n < i) return
     enddo
     toks = toks + 1
     ntokens = toks
     do
       i = i + 1
       if (n < i) return
       if (line(i:i) == ' ') exit
     enddo
  enddo
  end function ntokens 

  subroutine readDataFile
    use data, only: dp, expDataLength,expSignal,input_file
    implicit none
    integer :: i,imax
    integer,parameter :: maxlines = 100000000
    integer :: status
    real(dp) :: tmp
    real(dp) :: expSignal_int

    open(unit=4,file=trim(input_file),status='old',action='read',iostat=status)
    rewind(4)
    read(2,*)
    do i = 1,maxlines
      read(2,*,iostat=status)
      if(status/=0) exit
      imax = i
    end do

    expDataLength = imax

    allocate(expSignal(imax,2))

    rewind(2)
    do i = 1,imax
      read(4,*) expSignal(i,1),expSignal(i,2)
    end do

    do i = 1,(imax - 1)
      tmp = 0.5d0*(expSignal(i+1,1)-expSignal(i,1))*(expSignal(i+1,2)+expSignal(i,2))
      expSignal_int = expSignal_int + tmp
    end do
  end subroutine readDataFile
  subroutine writeData(file_name)
    use data, only : kmax,maxnl,particle,k,S,X_ann,dt
    implicit none
    integer :: i
    integer :: status
    character(len=100),intent(in) :: file_name

    open(unit=100,file=trim(file_name),status='replace',action='write',iostat=status)
    rewind(100)

    write(100,*)'Time [s]  Temperature [K] Diameter [cm]   Mass [g]        Signal  X_annealed'
    do i = 1,kmax-5!maxnl
      !print *,real(i),dt,(real(i)*dt)
      write(100,*) ((real(i)-1)*dt),particle%T(i,maxnl),particle%D(i),particle%M(i),S(i),X_ann(i)
      !write(100,*) i,particle%T(k,i),particle%phase(k,i)
    enddo
    close(100)

    open(unit=101,file='layerData.dat',status='replace',action='write',iostat=status)
    rewind(101)

    !write(101,*)'Time [s]  Temperature [K] Diameter [cm]   Mass [g]'
    do i = 1,maxnl
      !print *,real(i),dt,(real(i)*dt)
      !write(100,*) ((real(i)-1)*dt_tmp),particle%T(i,maxnl),particle%D(i),particle%M(i),S(i)
      write(101,*) i,particle%T(k,i),particle%phase(k,i)
    enddo
    close(101)
  end subroutine writeData
  subroutine readLsrProf
    use data, only : dp,q_exp,q_exp_int,LsrProf,Len_LsrProf
    implicit none
    integer :: i,imax
    integer :: maxlines = 1000000000
    integer :: status
    real(dp) :: tmp
    real(dp) :: scl = 1.0d-9

    ! Read laser profile from file

    open(unit=2,file=trim(LsrProf),status='old',action='read',iostat=status)
    rewind(2)
    read(2,*)
    do i = 1, maxlines
      read(2,*,iostat=status)
      if(status/=0) exit
      imax = i
    enddo

    allocate(q_exp(imax,2))

    rewind(2)
    read(2,*)
    do i = 1, imax
      read(2,*) q_exp(i,1),q_exp(i,2)
    enddo
    do i = 1,imax
      q_exp(i,1) = q_exp(i,1)*scl
    enddo
    close(2)
    Len_LsrProf = imax
    !time_final = q_exp(len_lsrprof,1)
    ! Integrate experimental laser profile 
    ! using trapezoid rule for numerical integration

    do i = 1,(Len_LsrProf-1)
      tmp = 0.5d0*(q_exp(i+1,1)-q_exp(i,1))*(q_exp(i+1,2)+q_exp(i,2))
      q_exp_int = q_exp_int + tmp
    end do
  end subroutine readLsrProf

  subroutine printstartinfo()
    implicit none
    print *,
    print *,'    Begin time integration' 
    print *,
  end subroutine
  subroutine printendinfo()
    implicit none
    print *,
    print *,'    Time integration complete'
    print *,
  end subroutine 
  subroutine printfinalinfo()
    use data, only: particle
    implicit none
    print *,'    Peak particle temperature ',MAXVAL(particle%T)
    print *,
  end subroutine
end module io

module utilities
implicit none

contains
  function vol(i)
    use data, only : dp,delta,pi
    implicit none
    integer :: i
    real(dp) :: vol
    real(dp) :: c1
    real(dp) :: D
    real(dp) :: r
    c1 = 4.0_dp/3.0_dp
    r = real(i)*delta
    D = 2.0_dp*r
    vol = c1*pi*(r**3)
  end function

    function lin_interpolate(y1,y2,x1,x2,x_val)
    ! lin_interpolate = y1 + (y2-y1)*(x_val-x1)/(x2-x1)
    use data, only: dp
    implicit none
    real(dp) :: lin_interpolate
    real(dp),intent(in) :: y1,y2,x1,x2,x_val
    lin_interpolate = y1+(y2-y1)*((x_val-x1)/(x2-x1))
  end function lin_interpolate

  subroutine computeLIIsignal
    use data, only : S,particle,k,maxnl,h, &
                 c,kb,dp,pi,colLambdai, &
                 colLambdaj
    use models, only : E
    implicit none
    real(dp) :: c1
    real(dp) :: c2
    integer :: i,imax
    real(dp) :: dLambda = 0.1d-7
    real(dp) :: lambdai,lambdaj
    real(dp) :: tmp = 0.0_dp
    real(dp) :: tmp2 = 0.0_dp
    imax = ceiling((colLambdaj - colLambdai)/dLambda)
    tmp = 0.0_dp
    tmp2 = 0.0_dp
    do i = 1, imax
      lambdai = colLambdai + (real(i)-1.0_dp)*dLambda
      lambdaj = colLambdai + real(i)*dLambda
      tmp = 0.5_dp*dLambda*(signalf(lambdaj)+signalf(lambdai))
      tmp2 = tmp2 + tmp
    end do
    S(k+1) = pi*(particle%D(k)**2)*tmp2
    contains
      function signalf(lambda)
        real(dp) :: signalf
        real(dp),intent(in) :: lambda
    
        c1 = E(lambda,1)*2.0_dp*pi*h*c**2
        c2 = (lambda**5)*(exp((h*c)/(lambda*kb*particle%T(k,maxnl)))-1.0_dp)
        signalf = c1/c2
      end function signalf
  end subroutine

  subroutine generategaussianprofile(d,b,c,a,out_file)
    implicit none
    integer :: i
    integer :: imax = 6000
    character(len=50) :: out_file
    real(8) :: end_time
    real(8) :: dx
    real(8) :: x
    real(8) :: a,b,c,d
    !print *,
    !write(*,'(A)')' Enter end time [ns]'
    !read(*,*) end_time
    end_time = (b+c)*2.0d0
    dx = end_time/real(imax) 
    x = 0.0d0
    open(unit=1,file=trim(out_file),status='replace',action='write')
    rewind(1)
    do i = 1,imax
      write(1,*) x,f(x,d,b,c,a)
      x = x + dx 
    end do
    close(1)
  contains
    function f(x,d,b,c,a)
      implicit none
      real(8) :: f
      real(8) :: x
      real(8) :: d!d = 0.d0
      real(8) :: b!b = 15.0d0
      real(8) :: c!c = 1.0d0
      real(8) :: a!a = 1.0d0
      f = a*exp(-((x-b)**2)/(2.0d0*(c**2)))+d
    end function
  end subroutine 

  subroutine initialize()
    use data
    use thermo, only: rho
    implicit none
    integer :: i

    ! Initialize constant properties
    particle%G = 0.0_dp 
    particle%X_ann = 0.0_dp
    particle%X_melt = 0.0_dp
    particle%phase = 'sol'

    S = 0.0_dp
    
    X_ann = 0.0_dp
    X_melt = 0.0_dp


    ! Initialize variables 
    M = vol(maxnl)*rho(T,maxnl,1)
    N_p = (M*N_a)/W_1

    ! Initialize particle arrays
    particle%T = T
    particle%D = D
    particle%M = M
    particle%delta = delta
    
    T_layer = T 
    
    N_d = X_d*N_p

    ! Initialize sublimation constants
    delH_lambda_a(1) = 7.2d5
    delH_lambda_a(2) = 3.4d5
    delH_lambda_a(3) = 7.7d4
    delH_lambda_s(1) = 7.2d5
    delH_lambda_s(2) = 3.4d5
    delH_lambda_s(3) = 7.7d4
    do i = 4,10
      delH_lambda_a(i) = 7.7E4_dp
      delH_lambda_s(i) = 7.7E4_dp
    enddo

    sigmaj(1) = 1.2d-15
    sigmaj(2) = 2.4d-15
    sigmaj(3) = 4.5d-15
    sigmaj(4) = 5.6d-15
    sigmaj(5) = 7.1d-15
    sigmaj(6) = 7.2d-15
    sigmaj(7) = 7.0d-15
    sigmaj(8) = 10.9d-15
    sigmaj(9) = 8.7d-15
    sigmaj(10) = 9.3d-15

    A0(1) = 9.0235d-4
    A0(2) = 5.7683d-4
    A0(3) = 1.0811d-4
    A1(1) = 3.8475d-7
    A1(2) = 1.8429d-7
    A1(3) = 5.1519d-7

    b0H(1) = 7.266E+5_dp
    b0H(2) = 8.545E+5_dp
    b0H(3) = 8.443E+5_dp
    b0H(4) = 9.811E+5_dp
    b0H(5) = 9.898E+5_dp
    b0H(6) = 13.56E+5_dp
    b0H(7) = 13.45E+5_dp
    b0H(8) = 15.80E+5_dp
    b0H(9) = 16.11E+5_dp
    b0H(10) = 14.99E+5_dp

    b1H(1) = -5.111_dp
    b1H(2) = -12.326_dp
    b1H(3) = -26.921_dp
    b1H(4) = -7.787_dp
    b1H(5) = -7.069_dp
    b1H(6) = 0.0_dp
    b1H(7) = 0.0_dp
    b1H(8) = 0.0_dp
    b1H(9) = 0.0_dp
    b1H(10) = 0.0_dp

    b2H(1) = 0.0_dp
    b2H(2) = 0.0_dp
    b2H(3) = 0.0_dp
    b2H(4) = -2.114E-3_dp
    b2H(5) = -2.598E-3_dp
    b2H(6) = 0.0_dp
    b2H(7) = 0.0_dp
    b2H(8) = 0.0_dp
    b2H(9) = 0.0_dp
    b2H(10) = 0.0_dp

    b0S(1) = 160.01_dp
    b0S(2) = 202.62_dp
    b0S(3) = 225.12_dp
    b0S(4) = 217.42_dp
    b0S(5) = 227.38_dp
    b0S(6) = 264.6_dp
    b0S(7) = 265.3_dp
    b0S(8) = 293.4_dp
    b0S(9) = 301.3_dp
    b0S(10) = 260.5_dp

    b1S(1) = -1.553E-3_dp
    b1S(2) = -7.584E-3_dp
    b1S(3) = -16.131E-3_dp
    b1S(4) = -9.619E-3_dp
    b1S(5) = -10.049E-3_dp
    b1S(6) = 0.0_dp
    b1S(7) = 0.0_dp
    b1S(8) = 0.0_dp
    b1S(9) = 0.0_dp
    b1S(10) = 0.0_dp

    b2S(1) = 0.0_dp
    b2S(2) = 5.246E-7_dp
    b2S(3) = 10.9E-7_dp
    b2S(4) = 4.089E-7_dp
    b2S(5) = 3.622E-7_dp
    b2S(6) = 0.0_dp
    b2S(7) = 0.0_dp
    b2S(8) = 0.0_dp
    b2S(9) = 0.0_dp
    b2S(10) = 0.0_dp

  end subroutine
  subroutine allocatearrays()
    use data, only: dp,maxlayers,D,maxnl,kmax,time_final,dt,kmax_layer, &
                    dt_layer,S,N_d,X_ann,X_melt,particle,T_layer
    implicit none

    ! Size Arrays
    maxlayers = ceiling((D/2.0_dp))
    maxnl = maxlayers
    kmax = ceiling(time_final/dt)+2
    kmax_layer = ceiling(dt/dt_layer)+2


    ! Allocate arrays
    allocate(S(kmax))
    allocate(N_d(kmax))
    allocate(X_ann(kmax))
    allocate(X_melt(kmax))
    allocate(T_layer(kmax_layer,maxnl))
    allocate(particle%G(kmax,maxnl))
    allocate(particle%T(kmax,maxnl))
    allocate(particle%D(kmax))
    allocate(particle%M(kmax))
    allocate(particle%X_ann(kmax))
    allocate(particle%X_melt(kmax))
    allocate(particle%phase(kmax,maxnl))
    allocate(particle%delta(kmax,maxnl))

  end subroutine
end module

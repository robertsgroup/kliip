module menu
use data, only: dp,input_file
implicit none
character(len=200) :: input

contains
  subroutine mainmenu(input,sim)
    implicit none
    character(len=200),intent(inout) :: input
    integer,intent(out) :: sim
    do
      call prompt()
      read(*,*) input
      if(trim(input)=='exit') then
        call quit()
      elseif(trim(input)=='help') then
        call help()
        call prompt()
        read(*,*) input
      elseif((trim(input)=='simulate') .or. (trim(input)=='sim')) then
        call simulatemenu()
        sim = 1
        exit
      elseif(trim(input)=='fit') then
        call fitmenu()
        sim = 2
        exit
      elseif(trim(input)=='generate') then
        call generatelaserprofile()
        exit
      else
        call unknowninput()
      end if
    end do
  end subroutine
  subroutine simulatemenu()
    implicit none
    print *,
    write(*,'(A)')'Enter input file (y/n)?'
      call prompt()
      read(*,*) input
      if(y_n(input)) then
        print *,
        write(*,'(A)')'Enter input file name'
        call prompt()
        read(*,*) input_file
      else
        write(*,'(A)')'Would you like to view the default parameters (y/n)?'
        call prompt()
        read(*,*) input
        if(y_n(input)) then
          call printDefaults()
        end if
        print *,
        write(*,'(A)')'Simulating temporal particle response with default parameters'
      end if
  end subroutine
  subroutine fitmenu()
    implicit none
    print *,
    write(*,'(A)')'Enter LII data file'
    read(*,*) input_file
  end subroutine
  subroutine info()
    implicit none
    print *,
    print *,
    print *,'       K L I I P'
    print *,'       Version 0.1 patchlevel 1    last modified August 28, 2014'
    !print *,'       Build System:'
    print *,
    print *,'       Copyright (C) 2014'
    print *,'       Joel Lisanti, Emre Cenker and others'
    print *,
    print *,'       kliip home:     http://ccrc.kaust.edu.sa'
    print *,'       bugs,questions: joel.lisanti@kaust.edu.sa'
    print *,
    print *,'       faq, help, info, etc: type "help"'
    print *,
    print *,'       cite: OUR PUBLICATION CITATION HERE'
    print *, 
  end subroutine 
  subroutine help()
    implicit none
    print *,
    print *,
    print *,' KLIIP is a portable Laser Induced Incandescence (LII) post-processing and'
    print *,' experiment design tool. The two primary functions are simulating the temporal'
    print *,' repsonse of a carbonous particle to laser aborption for measurement design'
    print *,' and fitting of an experimental signal obtained from an incadescent partile'
    print *,
    print *,' KLIIP stands for KAUST LII Post-processing as it is developed at the Clean'
    print *,' Combustion Research Center in King Abdullah University of Science and' 
    print *,' Technology'
    print *, 
    print *,'       simulate     generate a LII signal'
    print *,'       fit          post-process LII data'
    print *,'       generate     generate a laser profile'
    print *,'       exit         quit KLIIP'
    !print *,
    !print *,'       '
    !print *,'       '
    !print *,
    !print *,'       '
    !print *,'       '
    print *, 
  end subroutine
  subroutine prompt()
    implicit none
    write(*, '(A)', ADVANCE = "NO") "kliip> "
  end subroutine
  function y_n(val)
    implicit none
    character(len=200) :: val
    logical :: y_n
    do
      if((trim(val)=='y') .or. (trim(val)=='Y') .or. (trim(val)=='yes') .or.&
          (trim(val)=='Yes') .or. (trim(val)=='YES')) then
        y_n = .true.
        exit
      elseif((trim(val)=='n') .or. (trim(val)=='N') .or. (trim(val)=='no') .or.&
          (trim(val)=='No') .or. (trim(val)=='NO')) then
        y_n = .false.
        exit
      elseif(trim(input)=='exit') then
        call quit()
      else
        call unknowninput()
        call prompt()
        read(*,*) input
      end if
    end do
  end function
  subroutine generatelaserprofile()
    use utilities, only: generategaussianprofile
    implicit none
    character(len=50) :: file_name
    real(8) :: a,b,c,d
    print *, 
    write(*,'(A)')'Gaussian profile (y/n)?'
    call prompt()
    read(*,*) input
    if(y_n(input)) then
      print *,
      write(*,'(A)')'Enter asymptotic value'
      call prompt()
      read(*,*) d
      print *,
      write(*,'(A)')'Enter peak time [ns]'
      call prompt()
      read(*,*) b 
      print *,
      write(*,'(A)')'Enter standard deviation [ns]'
      call prompt()
      read(*,*) c 
      print *,
      write(*,'(A)')'Enter peak value'
      call prompt()
      read(*,*) a 
      print *,
      write(*,'(A)')'Enter output file name'
      call prompt()
      read(*,*) file_name
      call generategaussianprofile(d,b,c,a,file_name)
    else
      print *,
      print *, ' No other option currently available'
      print *,
    end if
  end subroutine
  subroutine unknowninput()
    implicit none
    !print *,
    print *,'       ^ Unrecognized input, type "help" for more info or "exit" to quit'
    print *,
  end subroutine
  subroutine quit()
    implicit none
    stop
  end subroutine
  subroutine printdefaults()
    implicit none
  end subroutine
end module

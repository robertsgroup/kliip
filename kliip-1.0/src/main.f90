program main
use menu
use data
use solve
use io
use utilities
implicit none

! Read input file name from argument

if (COMMAND_ARGUMENT_COUNT() /= 1) then
  use_menu = .true.
  call GET_COMMAND_ARGUMENT(1,input_file)
else
  use_menu = .false.
end if

call info()
do 
  if(use_menu) then
    call mainmenu(input,sim)
  else
    sim = 1
  end if

  select case (sim)
    case(1)

      ! Read input file
      call readinputfile()

      ! Read in laser profile
      call readLsrProf()

      ! Allocate arrays
      call allocatearrays()

      ! Initialize variables
      call initialize()
      
      ! Perform time integration 
      call timeIntegrate()

      call printfinalinfo()
      ! Write output data
      call writeData(out_file)
      close(11)

    case(2)
  end select
  if(use_menu) then
  else
    exit
  end if
enddo

end program main

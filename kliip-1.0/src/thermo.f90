module thermo
implicit none

contains
  function rho(Temp,layer,time_i)
    use errorCheck
    use data, only : dp,particle
    implicit none
    integer :: layer
    integer :: time_i
    real(dp) :: rho
    real(dp) :: Temp 

    if(particle%phase(time_i,layer)=='sol') then
      rho = 2.3031d0 - (7.3106d-5)*Temp
      if(rho<0.0d0) then
        error=500
        call errorFunc
      endif
    elseif(particle%phase(time_i,layer)=='liq') then
      rho = 2.0448d0 - (7.0809d-5)*Temp
      if(rho<0.0d0) then
        error=505
        call errorFunc
      endif
    elseif(particle%phase(time_i,layer)=='ann') then
      rho = 2.6d0 - (1.0d-4)*Temp
      if(rho<0.0d0) then
        error=510
        call errorFunc
      endif
    else
      error = 550
      call errorFunc
    endif
  end function
 
  function rhos(Temp)
    use errorCheck
    use data, only : dp
    implicit none
    real(dp) :: rhos
    real(dp) :: Temp 
    rhos = 2.3031d0 - (7.3106d-5)*Temp
    if(rhos<0.0d0) then
      error=500
      call errorFunc
    endif
  end function rhos

  function rhol(Temp)
    use errorCheck
    use data, only : dp
    implicit none
    real(dp) :: rhol
    real(dp) :: Temp
    rhol = 2.0448d0 - (7.0809d-5)*Temp
    if(rhol<0.0d0) then
      error=505
      call errorFunc
    endif
  end function rhol

  function rhoa(Temp)
    use errorCheck
    use data, only : dp
    implicit none
    real(dp) :: rhoa
    real(dp) :: Temp
    rhoa = 2.6d0 - (1.0d-4)*Temp
    if(rhoa<0.0d0) then
      error=510
      call errorFunc
    endif
  end function rhoa

  function kappas(temp)
    use data, only : dp
    implicit none
    real(dp) :: kappas
    real(dp) :: temp
    kappas = 16.985d0/temp
  end function

  function kappa_l(temp)
    use data, only : dp,R,lambda_e,m_e,W_1,kb 
    real(dp) :: kappa_l
    real(dp) :: temp
    real(dp) :: c1,c2,c3
    c1 = 3.0d0/8.0d0
    c2 = (rhol(temp)*R*lambda_e)/W_1
    c3 = sqrt((kb*temp)/m_e)
    kappa_l = c1*c2*c3
  end function 

  function cp(temp,layer,time_i)
    use data, only : dp,particle
    use errorCheck
    implicit none
    real(dp) :: temp
    real(dp) :: cp
    integer :: time_i
    integer :: layer
    if(particle%phase(time_i,layer)=='sol') then
      cp = c_s(temp)
    elseif(particle%phase(time_i,layer)=='liq') then
      cp = c_l(temp)
    else
      call errorFunc
    endif
  end function

  function c_layer(temp,layer,time_i)
    use data, only : dp,particle
    use errorCheck
    implicit none
    real(dp) :: temp
    real(dp) :: c_layer
    integer :: time_i
    integer :: layer
    if(particle%phase(time_i,layer)=='sol') then
      c_layer = c_s(temp)
    elseif(particle%phase(time_i,layer)=='liq') then
      c_layer = c_l(temp)
    else
      call errorFunc
    endif
  end function

  function c_s(Temp)
    use data, only : dp,R
    implicit none
    real(dp) :: c_s 
    real(dp) :: Temp
    real(dp) :: c1,c2,c3,c4,c5,c6
    real(dp),parameter :: a1 = 1.115d0
    real(dp),parameter :: a2 = 1.789d0
    real(dp),parameter :: a3 = 1.16d-4
    real(dp),parameter :: a4 = 12.01d0
    real(dp),parameter :: theta1 = 597.0d0
    real(dp),parameter :: theta2 = 1739.0d0
    c1 = theta1/Temp 
    c2 = theta2/Temp
    c3 = exp(c1)
    c4 = exp(c2)
    c5 = (c1**2)
    c6 = (c2**2)
    c_s = (R/a4)*((a1*c5*c3*((c3-1.0d0)**(-2)))+&
         (a2*c6*c4*((c4-1.0d0)**(-2)))+a3*Temp)
  end function c_s

  function c_l(Temp)
    use data, only : dp,R
    implicit none
    real(dp) :: c_l
    real(dp) :: Temp
    real(dp) :: c1,c2,c3,c4,c5,c6
    real(dp),parameter :: a1 = 4.5d0
    real(dp),parameter :: a2 = 0.0d0
    real(dp),parameter :: a3 = 0.0d0
    real(dp),parameter :: a4 = 12.01d0
    real(dp),parameter :: theta1 = 1280.0d0
    real(dp),parameter :: theta2 = 0.0d0
    c1 = theta1/Temp 
    c2 = theta2/Temp
    c3 = exp(c1)
    c4 = exp(c2)
    c5 = (c1**2)
    c6 = (c2**2)
    c_l = (R/a4)*((a1*c5*c3*((c3-1.0d0)**(-2)))+&
         (a2*c6*c4*((c4-1.0d0)**(-2)))+a3*Temp)
  end function c_l

  function C_p(temp)
    use data, only : dp,R
    implicit none
    real(dp) :: C_p
    real(dp) :: temp
    real(dp) :: c1,c2,c3,c4,c5,c6
    real(dp),parameter :: a1 = 3.498d0 
    real(dp),parameter :: a2 = 0.98378d0
    real(dp),parameter :: a3 = 2.5766d-5
    real(dp),parameter :: a4 = 1.0d0
    real(dp),parameter :: theta1 = 1.0d0
    real(dp),parameter :: theta2 = 3353.5d0
    c1 = theta1/Temp
    c2 = theta2/Temp
    c3 = exp(c1)
    c4 = exp(c2)
    c5 = (c1**2)
    c6 = (c2**2)
    C_p = (R/a4)*((a1*c5*c3*((c3-1.0d0)**(-2)))+&
         (a2*c6*c4*((c4-1.0d0)**(-2)))+a3*Temp)
  end function

  function C_pco(temp)
    use data, only : dp,R
    implicit none
    real(dp) :: C_pco
    real(dp) :: temp
    real(dp) :: c1,c2,c3,c4,c5,c6
    real(dp),parameter :: a1 = 3.494d0
    real(dp),parameter :: a2 = 0.98449d0
    real(dp),parameter :: a3 = 2.6164d-5
    real(dp),parameter :: a4 = 1.0d0
    real(dp),parameter :: theta1 = 1.0d0
    real(dp),parameter :: theta2 = 3085.1d0
    c1 = theta1/Temp
    c2 = theta2/Temp
    c3 = exp(c1)
    c4 = exp(c2)
    c5 = (c1**2)
    c6 = (c2**2)
    C_pco = (R/a4)*((a1*c5*c3*((c3-1.0d0)**(-2)))+&
         (a2*c6*c4*((c4-1.0d0)**(-2)))+a3*Temp)
  end function

  function k_rxn(A,E,Temp)
    use data, only : dp,R,neg
    implicit none
    real(dp) :: k_rxn
    real(dp) :: A
    real(dp) :: E
    real(dp) :: Temp
    k_rxn = A*exp((neg*E)/(R*Temp))
  end function
end module thermo

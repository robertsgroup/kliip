module radiation
implicit none

contains

!----------------------------------------------------------!
!
! Subroutine for Radiation modeling
!
!----------------------------------------------------------!

  subroutine computeQrad
    use data, only : Qrad,dp,pi,h,c,kb,eta,beta,T,D
    use models, only : l
    implicit none
    real(dp) :: c1,c2
    c1 = l(eta)*beta*(pi**2)*(D**3)&
          *((kb*T)**(4.0d0+eta))
    c2 = h*((h*c)**(2.0d0+eta))
    Qrad = c1/c2
  end subroutine

  function l(eta)
    use data, only : dp
    implicit none
    real(dp) :: eta
    real(dp) :: l
    !l = (4.0d0/3.0d0)*GAMMA(4.0d0+eta)*Z(40.d0+eta)
    l=(4.0d0/3.0d0)*EXP(1.862d0+1.99d0*(eta)+0.1909d0*(eta**2)&
        -0.02968d0*(eta**3))
  end function
end module

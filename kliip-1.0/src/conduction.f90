module conduction
implicit none

contains
!----------------------------------------------------------!
!
! Subroutine for conduction modeling
!
!----------------------------------------------------------!

  subroutine computeQcond
    use data
    use thermo, only : C_p
    implicit none
    !real(dp) :: alpha_T = 0.3d0
    real(dp) :: c1,c2
    Qcond = 0.0d0
    ! Need to add additional modeling and check for
    !  elevated pressures

    ! Knudsen regime
    c1 = (pi*(D**2)*alpha_T*Z_surf(T_bath,P_o))/N_a
    c2 = (C_p(T_bath)-(R/2.0d0))*(T-T_bath)
    Qcond = c1*c2
  end subroutine

  function Z_surf(T,P)
    use data, only : dp,kp,R_m,pi,W_a
    implicit none
    real(dp) :: T,P
    real(dp) :: Z_surf
    real(dp) :: c1,c2
    c1 = P/(kp*T)
    c2 = sqrt((R_m*T)/((2.0d0)*pi*W_a))
    Z_surf = c1*c2
  end function
end module

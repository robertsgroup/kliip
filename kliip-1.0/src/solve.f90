module solve
use io
use utilities
implicit none

contains

  subroutine timeIntegrate
    use data
    implicit none
    ! Advance solution in time
    call printstartinfo()
    do
      k = k + 1
      k_layer = 0
      iter_count = iter_count + 1
      ! Check for final time
      if((time+dt)>=time_final) then
        call updateSolution(time,(time_final-time))
        call computeLIIsignal
        call printendinfo()
        exit
      else
        call updateSolution(time,dt)
        call computeLIIsignal
        if(iter_count == print_int) then
          iter_count = 0
        endif
      endif
      ! Solve internal particle heat transfer between layers
      time_layer = time
      time_layer_final = time + dt
      do
        k_layer = k_layer + 1
        time_layer = time_layer + dt_layer
        if((time_layer+dt_layer)>time_layer_final) then
          call updateInternalSolution(time_layer_final-time_layer)
          exit
        else
          call updateInternalSolution(dt_layer)
        endif
      enddo
      call update
      time = time + dt
    enddo

  end subroutine timeIntegrate


!-------------------------------------------------------------------------!
!
! Subroutines and functions for updating the global 
!   particle temperature
!
!-------------------------------------------------------------------------!

  subroutine updateSolution(time,dt)
    use data, only : dp,particle,X_ann,X_melt,N_d,k,sixth,third,maxnl, &
                     Qabs,Qrad,Qcond,Qann,Qsub,Qox,X_d,N_p,N_a,W_1
    implicit none
    real(dp) :: k1,k2,k3,k4
    real(dp) :: l1,l2,l3,l4
    real(dp) :: h1,h2,h3,h4
    real(dp) :: g1,g2,g3,g4
    real(dp) :: v1,v2,v3,v4 
    real(dp) :: delT 
    real(dp) :: delM
    real(dp) :: delNdef
    real(dp) :: delXm
    real(dp) :: delXa
    real(dp) :: time,dt

    ! Fourth order Runge-Kutta

    ! First stage of Runge-Kutta
    h1 = dt*dNdfunc(particle%T(k,maxnl),particle%M(k),N_d(k),X_ann(k))
    k1 = dt*dTfunc(time,particle%T(k,maxnl),particle%M(k),X_ann(k),N_d(k))
    l1 = dt*dMfunc(time,particle%T(k,maxnl),particle%M(k),particle%D(k))
    g1 = dt*dXmeltfunc(time,particle%T(k,maxnl))
    v1 = dt*dXannfunc(time,particle%M(k),N_d(k))

    ! Second stage of Runge-Kutta
    h2 = dt*dNdfunc(particle%T(k,maxnl)+0.5d0*k1,particle%M(k)&
         +0.5d0*l1,N_d(k)+0.5d0*h1,X_ann(k)+0.5d0*v1)
    k2 = dt*dTfunc(time+0.5d0*dt,particle%T(k,maxnl)+0.5d0*k1,&
         particle%M(k)+0.5d0*l1,X_ann(k)+0.5d0*v1,N_d(k)+0.5_dp*h1)
    l2 = dt*dMfunc(time+0.5d0*dt,particle%T(k,maxnl)+0.5d0*k1,&
         particle%M(k)+0.5d0*l1,particle%D(k))
    g2 = dt*dXmeltfunc(time+0.5d0*dt,particle%T(k,maxnl)+0.5d0*k1)
    v2 = dt*dXannfunc(time+0.5d0*dt,particle%M(k)+0.5d0*l1,N_d(k)+0.5d0*h1)

    ! Third stage of Runge-Kutta
    h3 = dt*dNdfunc(particle%T(k,maxnl)+0.5d0*k2,particle%M(k)&
         +0.5d0*l2,N_d(k)+0.5d0*h2,X_ann(k)+0.5d0*v2)
    k3 = dt*dTfunc(time+0.5d0*dt,particle%T(k,maxnl)+0.5d0*k2,&
         particle%M(k)+0.5d0*l2,X_ann(k)+0.5d0*v2,N_d(k)+0.5_dp*h2)
    l3 = dt*dMfunc(time+0.5d0*dt,particle%T(k,maxnl)+0.5d0*k2,&
         particle%M(k)+0.5d0*l2,particle%D(k))
    g3 = dt*dXmeltfunc(time+0.5d0*dt,particle%T(k,maxnl)+0.5d0*k2)
    v3 = dt*dXannfunc(time+0.5d0*dt,particle%M(k)+0.5d0*l2,N_d(k)+0.5d0*h2)

    ! Fourth stage of Runge-Kutta
    h4 = dt*dNdfunc(particle%T(k,maxnl)+k3,particle%M(k)+l3,N_d(k)+&
         h3,X_ann(k)+v3)
    k4 = dt*dTfunc(time+dt,particle%T(k,maxnl)+k3,&
         particle%M(k)+l3,X_ann(k)+v3,N_d(k)+h3)
    l4 = dt*dMfunc(time+dt,particle%T(k,maxnl)+k3,&
         particle%M(k)+l3,particle%D(k))
    g4 = dt*dXmeltfunc(time+dt,particle%T(k,maxnl)+k3)
    v4 = dt*dXannfunc(time+dt,particle%M(k)+l3,N_d(k)+h3)

    ! Compute change in parameters 
    delNdef = sixth*h1+third*h2+third*h3+sixth*h4 
    delT = sixth*k1+third*k2+third*k3+sixth*k4 
    delM = sixth*l1+third*l2+third*l3+sixth*l4 
    delXm = sixth*g1+third*g2+third*g3+sixth*g4 
    delXa = sixth*v1+third*v2+third*v3+sixth*v4 

    ! Updated parameters
    N_p = (particle%M(k+1)*N_a)/W_1
    N_d(k+1) = N_d(k) + delNdef
    X_ann(k+1) = 1.0_dp-((N_d(k+1))/(X_d*N_p))
    particle%T(k+1,maxnl) = particle%T(k,maxnl) + delT
    particle%M(k+1) = particle%M(k) + delM
    !X_ann(k+1) = X_ann(k) + delXa
    X_melt(k+1) = X_melt(k) + delXm
    write(11,*) (time*1.0d9),Qabs,Qrad,Qcond,Qsub,Qann,Qox,N_d(k)
  end subroutine

  function dTfunc(timestep,temperature,mass,Xannealed,num_defects)
    use data, only : dp,D,pi,Qabs_on,Qrad_on, &
                     Qcond_on,Qann_on,Qsub_on,Qox_on, &
                     Qabs,Qrad,Qcond,Qsub,Qann,Qox, &
                     T,M,Xa,timek,maxnl,k,num_def
    use thermo, only : Cp,rho,c_layer
    use absorption
    use radiation
    use conduction
    use sublimation
    use annealing
    use oxidation
    implicit none
    real(dp) :: dTfunc 
    real(dp),intent(in) :: timestep
    real(dp),intent(in) :: temperature
    real(dp),intent(in) :: mass
    real(dp),intent(in) :: Xannealed
    real(dp),intent(in) :: num_defects
    real(dp) :: c1
    timek = timestep
    T = temperature
    M = mass
    Xa = Xannealed 
    num_def = num_defects
   
    if(Qabs_on)  call computeQabs
    if(Qrad_on)  call computeQrad
    if(Qcond_on) call computeQcond
    if(Qsub_on) call computeQsub
    if(Qann_on)  call computeQann
    if(Qox_on)   call computeQox
    
    !print *,'Qann',Qann
    c1 = 6.0d0/(pi*(D**3)*rho(T,maxnl,k)*c_layer(T,maxnl,k))
    dTfunc = c1*(Qabs-Qrad-Qcond-Qsub+Qann+Qox)
  end function
  
  function dMfunc(timestep,temperature,mass,diameter)
    use data, only : dp,timek,T,M,D
    use sublimation, only: dMdt
    use oxidation, only: dMdt_ox
    implicit none
    real(dp) :: timestep
    real(dp) :: temperature
    real(dp) :: mass
    real(dp) :: diameter
    real(dp) :: dMfunc
    integer :: j
    timek = timestep
    T = temperature
    M = mass
    D = diameter
    dMfunc = 0.0_dp
    do j = 1,10
      dMfunc = dMfunc + dMdt(j) 
    end do
    dMfunc = dMfunc + dMdt_ox(D,T)
    
  end function

  function dNdfunc(temperature,mass,Ndeffects,Xannealed)
    use data, only : dp,Xa,N_a,W_1,N_p,T,Ndef,M,X_d,Adiss, &
                     Ediss,Aimig,Eimig,Avmig,Evmig
    use thermo, only : k_rxn
    implicit none
    real(dp) :: dNdfunc
    real(dp) :: temperature
    real(dp) :: mass
    real(dp) :: Ndeffects
    real(dp) :: Xannealed
    real(dp) :: c1,c2,c3
    Xa = Xannealed
    T = temperature
    M = mass
    Ndef = Ndeffects
    N_p = (M*N_a)/W_1
    Xa = 1.0_dp-(Ndef/(X_d*N_p))
    c1 = Xa*(N_p/2.0d0)*k_rxn(Adiss,Ediss,T)
    c2 = k_rxn(Aimig,Eimig,T)*Ndef
    c3 = k_rxn(Avmig,Evmig,T)*Ndef
    dNdfunc = c1-c2-c3
  end function

  function dXmeltfunc(timestep,temperature)
    use data, only : T,dp,timek
    implicit none
    real(dp) :: dXmeltfunc
    real(dp) :: timestep
    real(dp) :: temperature
    timek = timestep
    T = temperature
    !do i = 1,maxnl 
    !   
    !enddo
    !dXmeltfunc = (Xm-X_melt(k))/(timek-time)
    dXmeltfunc = 0.0d0
  end function

  function dXannfunc(timestep,mass,Ndeffects)
    use data, only : M,dp,X_d,X_ann,k,N_a,W_1,N_p
    implicit none
    real(dp) :: dXannfunc
    real(dp) :: timestep
    real(dp) :: mass
    real(dp) :: Ndeffects
    real(dp) :: c1,c2
    mass = mass
    !timek = timestep
    timestep = timestep
    !M = mass
    !Ndef = Ndeffects
    N_p = (M*N_a)/W_1
    !Xa = 1.0d0-(Ndef/(X_d*N_p)) 
    !print *,M,Ndef,N_p,Xa
    !if(timek-time<1.0d-14) then
    !  dXannfunc = 0.d0
    !else
    !  dXannfunc = (Xa-X_ann(k))/(timek-time)
    !endif
    !print *,'dXannfunc',dXannfunc
    !print *,Xa,X_ann(k),timek,time,timek-time
    !Xa = 0.0d0
    c2 = 1.0_dp-(Ndeffects/(X_d*N_p))
    c1 = X_ann(k)
    dXannfunc = 0.0_dp !(c2-c1)/timestep 
    !print *,c2,c1,timestep
    !print *,dXannfunc
  end function


!----------------------------------------------------------!
!
! Subroutines and functions for updating the internal
!  particle temperature
!
!----------------------------------------------------------!

  subroutine updateInternalSolution(timestep)
    use thermo, only : rho,rhos,c_s,c_layer
    use utilities, only : vol
    use data
    use internalEnergy
    implicit none
    real(dp) :: delT
    real(dp),intent(in) :: timestep
    integer :: layer
    !real(dp) :: delG
    !real(dp) :: Gmelt
    real(dp) :: c1
    T_layer(k_layer,maxnl) = particle%T(k,maxnl) 

    do layer = (maxnl-1),1,-1
      call computeQbulk(layer)
      c1 = Qbulk/(rho(particle%T(k,layer),layer,k)*c_layer(particle%T(k,layer),layer,k))
      c1=Qbulk/(rhos(T_layer(k_layer,layer))*c_s(T_layer(k_layer,layer)))
      !print *,rhos(particle%T(k,layer)),c_s(particle%T(k,layer))
      delT = c1*timestep
      !if((delT+particle%T(k,layer))>=layer_melting_T) then
      !  particle%phase(k+1,layer) = 'liq'
      !  particle%T(k+1,layer) = layer_melting_T
      !  delG = Qbulk*vol(layer)*dt
      !  particle%layerG(k+1,layer) = particle%layerG(k,layer) + delG 
      !  Gmelt = (delH_fus*vol(layer)*rho(particle%T(k+1,layer),layer,k))/W_1 
      !  if(particle%layerG(k+1,layer)>=Gmelt) then
      !  endif
     ! else
        T_layer(k_layer+1,layer) = T_layer(k_layer,layer) + delT
      !endif 
    end do
  end subroutine updateInternalSolution
  subroutine update
    use data
    implicit none
    integer :: layer
    !real(dp) :: c1,c2,c3,c4
    !real(dp) :: temp

    !temp = particle%T(k,maxnl)
    !c1 = (6.0d0*particle%M(k))/pi
    !c2 = (X_ann(k)/rhoa(temp))
    !c3 = (X_melt(k)/rhol(temp))
    !c4 = ((1.0d0-X_ann(k)-X_melt(k))/rhos(temp))

    !particle%D(k+1) = (c1*(c2+c3+c4))**third 

    do layer = 1,(maxnl-1)
      particle%T(k+1,layer) = T_layer(k_layer+1,layer)
      T_layer(1,layer) = T_layer(k_layer+1,layer)
    enddo
  end subroutine update
end module

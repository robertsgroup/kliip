module annealing
implicit none

contains
  subroutine computeQann
    use data, only: N_a,dp,delH_imig,delH_vmig,T, &
                    Qann,Aimig,Eimig,Avmig,Evmig,num_def
    use thermo, only : k_rxn
    implicit none
    real(dp) :: c1,c2
    !Qann = 0.0d0
    !N_p = (particle%M(k)*N_a)/W_1
    !N_d(k) = computeN_d(k)
    !X_ann(k) = 1.0d0-(N_d(k)/(X_d*N_p))
    !Qann = (-delH_fus/N_a)*dX_anndt(k)
    c1 = -delH_imig*k_rxn(Aimig,Eimig,T)
    c2 = -delH_vmig*k_rxn(Avmig,Evmig,T)
    !print *,c1,c2,T,N_d(k)
    !print *,(c1+c2),N_a
    Qann = ((c1+c2)*num_def)/N_a
    !print *,((c1+c2)/N_a),Qann
    !print *,delH_imig,delH_vmig,Aimig,Eimig,Avmig,Evmig,Qann
    !Qann = 0.0_dp
    !stop
  end subroutine
end module

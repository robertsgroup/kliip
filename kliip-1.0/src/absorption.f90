module absorption
implicit none

contains
  subroutine computeQabs
    use errorCheck
    use data, only : Qabs,dp,Xa,time
    implicit none
    real(dp) :: Cabs,Cabs_ann,Cabs_unann
    integer::phase

    phase = 0
    call computeCabs(Cabs,phase)
    Cabs_unann = Cabs

    phase = 1
    call computeCabs(Cabs,phase)
    Cabs_ann = Cabs
    Cabs = (1.0_dp-Xa)*Cabs_unann + Xa*Cabs_ann

    Qabs = Cabs*q(time)

    if(ISNAN(Qabs)) then
      error = 300
      call errorFunc
    elseif(Qabs<0.0d0) then
      error = 301
      call errorFunc
    endif
  end subroutine
   
  subroutine computeCabs(Cabs,phase)
    use data, only : dp,D,lambda,pi
    implicit none
    real(dp),intent(out)::Cabs
    integer,intent(in)::phase
    Cabs = ((pi**2)*(D**3)*E(lambda,phase))/lambda
  end subroutine computeCabs

  function E(lambda,phase)
    use data, only : dp
    implicit none
    real(dp),intent(in)::lambda
    integer,intent(in)::phase
    real(dp) :: E
    real(dp) :: c1,c2,c3

    c1 = 6.0d0*nm(lambda,phase)*km(lambda,phase)
    c2 = (((nm(lambda,phase))**2)-((km(lambda,phase))**2)+2.0_dp)**2
    c3 = 4.0_dp*((nm(lambda,phase))**2)*((km(lambda,phase))**2)
    E = c1/(c2+c3)
  end function

  function nm(lambda,phase)
    use data, only : dp,nm_s,particle,k
    implicit none
    real(dp) :: lambda
    real(dp) :: nm
    integer :: phase
    if(phase == 0) then
      if(particle%X_melt(k) > 0.9_dp) then
        ! liquid phase
        nm = 1.26_dp 
      else
        ! unannealed phase
        nm = nm_s 
      end if
    else
      ! annealed phase
      nm = 2.213_dp + 9.551E3_dp*lambda
    end if
  end function

  function km(lambda,phase)
    use data, only : dp,km_s,particle,k
    implicit none
    real(dp) :: lambda
    real(dp) :: km
    integer :: phase
    if(phase == 0) then
      if(particle%X_melt(k) > 0.9_dp) then
        ! liquid phase
        km = 0.364_dp + (4.784E2*lambda) + (6.06E7*(lambda**2))
      else
        ! unannealed phase
        km = km_s
      end if
    else
      ! annealed phase
      km = 0.7528_dp + 1.265E4_dp*lambda
    endif
  end function

  function q(time)
    use data, only: dp,q_exp,len_lsrprof,F,q_exp_int,tol,q_e
    use errorCheck
    use utilities, only: lin_interpolate
    implicit none
    integer :: i
    real(dp) :: q
    real(dp) :: time

    do i=1,(len_lsrprof-1)
      if((time>=q_exp(i,1)) .and. (time<=(q_exp(i+1,1)+tol))) then
        q_e = lin_interpolate(q_exp(i,2),q_exp(i+1,2),q_exp(i,1),&
                              q_exp(i+1,1),time)
        q = (F*q_e)/q_exp_int
        if(q<0.0d0) then
          q = abs(q)
        endif
        exit
      else
        q = 0.0_dp
      endif
    enddo
    if(ISNAN(q)) then
      error = 100
      call errorFunc
    endif
  end function
end module

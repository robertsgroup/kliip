module data
implicit none
integer, parameter :: dp = selected_real_kind(15, 307)

! Menu
character(len=5) :: answr 
logical :: use_menu = .true.

! Data dictionary, solver
! Constants
integer,parameter :: print_int       = 100      ! Solution print interval
integer,parameter :: print_int_layer = 1000000  ! Solution print interval
real(dp),parameter :: tol            = 1.0d-20  ! Tolerance

! Variables
logical :: Qabs_on  = .false.
logical :: Qrad_on  = .false. 
logical :: Qcond_on = .false.
logical :: Qsub_on  = .false.
logical :: Qann_on  = .false.
logical :: Qox_on   = .false.
logical :: Qbulk_on = .false.

integer :: maxnl             = 0           ! Max number of particle layers at current time step
integer :: maxlayers         = 0           ! Initial max number of particle layers
integer :: k                 = 0           ! Maker for current time step
integer :: kmax              = 0
integer :: iter_count        = 0
real(dp) :: dt               = 0.0_dp      ! Global time step [s]
real(dp) :: dt_layer         = 0.0_dp
real(dp) :: time_final       = 0.0_dp
real(dp) :: time_layer       = 0.0_dp
real(dp) :: time_layer_final = 0.0_dp
real(dp) :: time             = 0.0_dp
real(dp) :: timek            = 0.0_dp

real(dp),allocatable,dimension(:) :: S     ! LII Signal

! Bath gas properties
real(dp) :: P_o = 0.0_dp                   ! System pressure [atm]
real(dp) :: T_bath = 0.0_dp

! Particle properties
real(dp) :: D    = 0.0_dp
real(dp) :: T    = 0.0_dp
real(dp) :: M    = 0.0_dp
real(dp) :: Ndef = 0.0_dp
real(dp) :: Xa   = 0.0_dp
real(dp) :: Xm   = 0.0_dp

type primary_particle
  real(dp),allocatable,dimension(:,:) :: T 
  real(dp),allocatable,dimension(:,:) :: G 
  real(dp),allocatable,dimension(:) :: D 
  real(dp),allocatable,dimension(:) :: M 
  real(dp),allocatable,dimension(:,:) :: delta
  real(dp),allocatable,dimension(:) :: X_ann 
  real(dp),allocatable,dimension(:) :: X_melt
  character(len=3),allocatable,dimension(:,:) :: phase
end type primary_particle

type (primary_particle) :: particle

!type layer
!  real(dp),allocatable,dimension(:,:) :: T
!  real(dp),allocatable,dimension(:,:) :: layerG
!  real(dp),allocatable,dimension(:) :: D
!  real(dp),allocatable,dimension(:) :: M
!  character(len=3),allocatable,dimension(:,:) :: phase
!end type layer
!type (primary_particle) :: particle


! Input files
!integer :: Len_LsrProf
character(len=200) :: LsrProf = 'laserprofile.dat'
character(len=200) :: out_file = 'output.dat'
character(len=200) :: input_file = 'input.in'

! Curve Fitting
integer :: expDataLength = 0
real(dp) :: chiS = 0.0_dp
real(dp),allocatable,dimension(:,:) :: computedSignal
real(dp),allocatable,dimension(:) :: expS
real(dp),allocatable,dimension(:,:) :: expSignal
integer :: sim = 0
real(dp) :: time_cooled_exp = 0.0_dp                   ! Time at which particle has returned to flame temperature
real(dp) :: time_cooled_sim = 0.0_dp
real(dp) :: tol_cooled = 0.0_dp

! Data dictioanry, internal energy
! Constants

! Variables
real(dp) :: Qint  = 0.0_dp                              ! Sensible energy storage
real(dp) :: Qabs  = 0.0_dp                             ! Laser energy absorption
real(dp) :: Qrad  = 0.0_dp                             ! Radiation rate
real(dp) :: Qcond = 0.0_dp                             ! Energy dissipation by conduction
real(dp) :: Qsub  = 0.0_dp                             ! Rate of energy loss by sublimation
real(dp) :: Qann  = 0.0_dp                            ! Rate of energy production by annealing
real(dp) :: Qox   = 0.0_dp                            ! Rate of energy generation by oxidation
real(dp) :: Qbulk = 0.0_dp                            ! Bulk internal heat transfer

! Data dictionary, absorption 
! Constants
real(dp) :: nm_s = 1.57_dp                    ! Real part of the index of refraction
real(dp) :: km_s = 0.56_dp                    ! Complex part of the index of refraction

! Variables
integer :: Len_LsrProf = 0
real(dp) :: F = 0.0_dp                                 ! Laser fluence [J/cm^2]
real(dp) :: q_e = 0.0_dp
real(dp) :: q_exp_int = 0.0_dp                       ! Integrated laser profile


! Data dictionary, radiation
! Constantss
real(dp) :: eta = 1.186d0
real(dp) :: beta = 0.626d0

! Data dictionary, conduction
! Constants

! Variabels

! Data dictionary, sublimation
! Constants
real(dp),dimension(10) :: b0H = 0.0_dp
real(dp),dimension(10) :: b1H = 0.0_dp
real(dp),dimension(10) :: b2H = 0.0_dp
real(dp),dimension(10) :: b0S = 0.0_dp
real(dp),dimension(10) :: b1S = 0.0_dp
real(dp),dimension(10) :: b2S = 0.0_dp
real(dp),dimension(10) :: delH_lambda_a = 0.0_dp
real(dp),dimension(10) :: delH_lambda_s = 0.0_dp
real(dp),dimension(3) :: A0 = 0.0_dp
real(dp),dimension(3) :: A1 = 0.0_dp
real(dp),dimension(10) :: sigmaj = 0.0_dp
! Variables

! Data dictionary, annealing
! Variables
real(dp),allocatable,dimension(:) :: N_d
real(dp) :: N_p = 0.0_dp
real(dp) :: num_def = 0.0_dp

! Constats
real(dp),parameter :: delH_imig = -1.9d4      ! Estimated enthalpy for interstitial migration [J/mol]
real(dp),parameter :: delH_vmig = -1.4d5      ! Enthalpy for vacancy migration [J/mol]
real(dp),parameter :: Aimig = 1.0d8
real(dp),parameter :: Eimig = 8.3d4
real(dp),parameter :: Avmig = 1.5d17
real(dp),parameter :: Evmig = 6.7d5
real(dp),parameter :: Adiss = 1.0d18
real(dp),parameter :: Ediss = 9.6d5
!real(dp),parameter :: X_d = 1.0d-2            ! Initial defect density

! Data dictinoary, oxidation
! Constants
real(dp),parameter :: delH_ox = -2.215d5      ! Enthalpy for oxidation [J/mol]
real(dp),parameter :: Aa = 5.0d23
real(dp),parameter :: Ea = 1.255d5
real(dp),parameter :: Az = 21.3d0
real(dp),parameter :: Ez = -1.713d4
real(dp),parameter :: Ab = 5.0d21
real(dp),parameter :: Eb = 6.352d4
real(dp),parameter :: At = 3.79d27
real(dp),parameter :: Et = 4.06d5

! Data dictionary, internal properties
real(dp),allocatable,dimension(:) :: X_ann
real(dp),allocatable,dimension(:) :: X_melt
real(dp),allocatable,dimension(:,:) :: q_exp  ! Experimental laser profile
real(dp),allocatable,dimension(:,:) :: T_layer
integer :: k_layer = 0
integer :: kmax_layer = 0
real(dp) :: layer_melting_T = 4500.0d0        ! Layer melting temperature [K]
real(dp) :: delta = 0.0_dp                             ! Particle internal layer thickness [cm]



! Data dictionary, constants
real(dp),parameter :: R = 8.3144621d0         ! Universal gas constant [J/mol*K]
real(dp),parameter :: R_m = 8.3145d7          ! Universal gas constant mass units [g*cm^2/mol*K*s^2]
real(dp),parameter :: R_p = 82.058d0          ! Universal gas constant pressure units [cm^3/mol*K]
real(dp),parameter :: neg = -1.0_dp           ! Constant
real(dp),parameter :: third = 1.0d0/3.0d0     ! Constant
real(dp),parameter :: sixth = 1.0d0/6.0d0     ! Constant
real(dp),parameter :: pi = 4.0d0*atan(1.0d0)  ! Pi
real(dp),parameter :: kb = 1.3806488d-23      ! Boltzman constant [J/K]
real(dp),parameter :: kp = 1.3626d-22         ! Boltzman constant [atm*cm^3/K]
real(dp),parameter :: c = 2.998d10            ! Speed of light [cm/s]
real(dp),parameter :: h = 6.625d-34           ! Planck constant [J*s]
real(dp),parameter :: lambda_e = 3.4d-8       ! Electron mean free path in liquid graphite
real(dp),parameter :: lambda_s = 500.0d-7
real(dp),parameter :: m_e = 9.109534d-35      ! Electron mass [J*s^2/cm^2]
real(dp),parameter :: W_1 = 12.011d0          ! Molecular weight of atomic carbon [g/mol]
real(dp),parameter :: W_a = 28.97d0           ! Mean molecular weight of air [g/mol]
real(dp),parameter :: delH_fus = 1.05d5       ! Heat of fusion for graphite [J/mol]
real(dp),parameter :: N_a = 6.02214d23        ! Avogadro constant [mol^-1]
real(dp),parameter :: X_d = 1.0d-2            ! Initial defect density
real(dp),parameter :: alpha_T = 0.3d0         ! Thermal accommodation coefficient
real(dp),parameter :: P_O2 = 0.209d0          ! Partial pressure of oxygen [atm]
real(dp),parameter :: Pref = 1.0d0            ! Reference pressure [atm]
real(dp),parameter :: Blambda_s = 0.4d0       ! [cm^4/J^2]
real(dp),parameter :: Blambda_a = 0.9d0       ! [cm^4/J^2]
real(dp),parameter :: Nsa = 3.8d15            ! [cm^-2]
real(dp),parameter :: Nss = 2.8d15            ! [cm^-2]
real(dp),parameter :: n = 2.0d0

! Data dictionary, variables 
real(dp) :: lambda = 0.0_dp
real(dp) :: colLambdai = 0.0_dp
real(dp) :: colLambdaj = 0.0_dp

contains
end module data

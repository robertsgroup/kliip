module fit
use data, only: dp
implicit none

contains
  subroutine biSection
    use data, only : dp,chiS,time_cooled_exp,time_cooled_sim,tol
    implicit none
    real(dp) :: a
    real(dp) :: b
    real(dp) :: c
    integer :: itr
    integer :: i
    integer :: maxit = 10000000

    a = 0.000000000011d0
    b = 1.0d0

    do i = 1,maxit
      c = (a + b)/2.0d0
      call chiSquared
      if(chiS < tol) then
        print *,' Primary particle diameter'
        print *,c
        exit
      endif
      itr = itr + 1
      if(itr == maxit) exit
      if(time_cooled_exp < time_cooled_sim) then
        a = c
      else
        b = c
      endif
    enddo

  end subroutine biSection
  subroutine chisquared
    use data, only: dp,expDataLength,chiS,computedSignal,expSignal
    implicit none
    integer :: i
    real(dp) :: tmp

    tmp = 0.0_dp

    do i = 1,expDataLength
      tmp = ((computedSignal(i,2)-expSignal(i,2))**2)/computedSignal(i,2)
      chiS = chiS + tmp
    end do
 
  end subroutine chisquared
end module fit
